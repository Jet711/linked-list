#include <stdio.h>
#include <stdlib.h>
#define MAX 10000

struct node
{
  int data;
  struct node *next;
}node;
struct node *head;
struct node *ap[]={NULL,NULL,NULL};

void insert_data(int data)
{
    struct node *ptr;
    int n,flag=1;

    while(flag==1)
    {
        
        printf("\nEnter the elements:");
        scanf("%d",&n);
        if(n<=0)
        {
            printf("\nNumber not accepted");
        }
        else
        {
            ptr=(struct node*)malloc(sizeof(node));
            ptr->data=n;
            ptr->next=ap[0];
            head = ap[0];
            ap[0]=ptr;
        }     
        printf("\nTo insert another element press '1' : ");
        scanf("%d", &flag);
    }  
}

void delete_data(int data)
{
    struct node *ptr;
    int pos,i;

    printf("\nEnter the position of the data to be deleted");
    scanf("%d",&pos);

    ptr=head;

    for(i=0;i<pos;i++)
    {
        ap[i]=ptr;
        ptr=ptr->next;

        if(ptr==NULL)
        {
             printf("There are less than %d elements,can't delete",pos);
             return;
        }
    }
    ap[0]->next=ptr->next;
    free(ptr);
    printf("Node Deleted....");
}

void display()
{
    struct node *ptr=(struct node*)malloc(sizeof(node));
    printf("\n Display the list...");
    printf("\nNULL\t");
    for (ptr=ap[0];ptr;ptr=ptr->next)
    {
        int e = ptr->data;
        printf("%d\t ",e);
    }
}

int main()
{
    int option=0,data;

    while(option!=4)
    {
        printf("\n-------Main Menu---------\n");
		printf("\n1. Insert the data");
		printf("\n2. Delete the data");
		printf("\n3. Display the list");
		printf("\n4. Exit");
		printf("\nEnter your option:");
		scanf("%d",&option);

        switch(option)
        {
            case 1:
				insert_data(data);
				break;
			case 2:
				delete_data(data);
				break;
			case 3:
				display();
				break;
			case 4:
				exit(0);
			    break;
			default:
				printf("Please enter the valid option....");
        }
    }
    return 0;
 }