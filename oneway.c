
#include<stdio.h>
#include<stdlib.h>

struct node{
    struct node *prev;
    int data;
    struct node *next;
};
struct node *head;

void insert_Start() // insert the data at starting point
{
    struct node *ptr;   // define the pointer
    int item;
    // alloct the memory 
    ptr = (struct node *)malloc(sizeof(struct node));   
	
	if(ptr == NULL)          // check overflow condition
    {  
       printf("\nOVERFLOW");  
    }  
    else                 // else ask to enter the data
    {
        printf("\nEnter the data");  
        scanf("%d",&item);  
        if(item<=0)
        {
        	printf(" number is not accepted....");
        	return;        	
		}       		
        if(head==NULL) 
        {
            ptr->next=NULL;
            ptr->prev=NULL;
            ptr->data=item;
            head=ptr;
        }
        else
        {
            ptr->data=item;  
            ptr->prev=NULL;  
            ptr->next = head;  
            head->prev=ptr;  
            head=ptr;   
        }
        printf("\nNode inserted\n");  
    }
}
void insert_end() // insert the data at ending point
{
  	struct node *ptr,*temp;  //define the pointer
  	int item;  
  	ptr = (struct node *) malloc(sizeof(struct node));  
  	//alocate the memory
  	if(ptr == NULL)  
  	{	  
    	printf("\nOVERFLOW");  //check if overflow
 	}  
  	else  
  	{	  
    	printf("\nEnter the data");  //ask for the value
    	scanf("%d",&item);  
    	if(item<=0)
    	{	
    		printf(" number is not accepted....");
    		return;
		}
		ptr->data=item; 
		if(head == NULL)  //check the head is empty 
    	{
			ptr->next = NULL;  
    		ptr->prev = NULL;  
      		head = ptr;  
    	}  
    	else  //if not then point to the next 
    	{
			temp = head;  
    	  	while(temp->next!=NULL)  
    	  	{
			  temp = temp->next;  
    	  	}  
    	    temp->next = ptr;  
    	  	ptr ->prev=temp;  
    	  	ptr->next = NULL;  
    	}
    	printf("\nnode inserted\n");  //display message 
	}              
}  

void insert_specific() // insert the data at specific point
{
	struct node *ptr,*temp;  
    int item,loc,i;  
    ptr = (struct node *)malloc(sizeof(struct node));  
    if(ptr == NULL)  
    {
    	printf("\n OVERFLOW");  
  	}  
  	else  
    {  
    	temp=head;  
    	printf("Enter the location");  
    	scanf("%d",&loc);  
    	for(i=0;i<loc;i++)  
    	{  
    		temp = temp->next;  
      		if(temp == NULL)  
      		{	  
        		printf("\n There are less than %d elements", loc);  
        		return;  
      		}  
    	}  
    	printf("Enter data");  
    	scanf("%d",&item);
		if(item<=0)
		{
			printf(" number is not accepted....");
    	    return;
		}
    	else    
    	ptr->data = item;  
    	ptr->next = temp->next;  
    	ptr -> prev = temp;  
    	temp->next = ptr;  
    	temp->next->prev=ptr;  
    	printf("\nnode inserted\n");  
  	}  
}  

void delete_start() // delete the data at starting point
{
	struct node *ptr;  
    if(head == NULL)  //check the condition of underflow
    {  
        printf("\n UNDERFLOW");  
    }  
    else if(head->next == NULL)  //if not then delete it (head)
    {  
        head = NULL;   
        free(head);  
        printf("\nnode deleted\n");  
    }  
    else              //if not then delete it(pointer)
    {  
        ptr = head;  
        head = head -> next;  
        head -> prev = NULL;  
        free(ptr);  
        printf("\nnode deleted\n");  
    }
	return; 
}
void delete_end() // delete the data at end point
{
	struct node *ptr;  
    if(head == NULL)  //check the underflow condition
    {  
        printf("\n UNDERFLOW");  
    }  
    else if(head->next == NULL)  //if not the delete the head
    {  
        head = NULL;   
        free(head);   
        printf("\nnode deleted\n");  
    }  
    else            //else delete the pointer 
    {  
        ptr = head;   
        if(ptr->next != NULL)  
        {  
            ptr = ptr -> next;   
        }  
        ptr -> next -> prev = NULL;
        ptr -> prev -> next = ptr->next;   
        free(ptr);  
        printf("\nnode deleted\n");  
    }  
    return;
}
void delete_specific() // delete the data at specific point
{
   struct node *ptr, *temp;  
    int val;  
    printf("\n Enter the data after which the node is to be deleted : ");  //asking for value to be deleted 
    scanf("%d", &val);  
    ptr = head;  
    while(ptr -> data != val)  
    //scan all the data until it match with value
    ptr = ptr -> next;  
    if(ptr -> next == NULL)  
    {  
        printf("\nCan't delete\n");  
    }  
    else if(ptr -> next -> next == NULL)  
    {  
        ptr ->next = NULL;  
    }  
    else  
    {   
        temp = ptr -> next;  
        ptr -> next = temp -> next;  
        temp -> next -> prev = ptr;  
        free(temp);  
        printf("\nnode deleted\n");  
    }
    
} 

void display() // will display it
{
    struct node *ptr;  
    printf("\n printing values...\n");  
    ptr = head;  
    while(ptr != NULL)  
    {  
        printf("%d->",ptr->data);  
        ptr=ptr->next;  
    }
	printf("NULL");  
}

int main()
{
	int option;
	while(option!=8)
	{
		printf("\n-------Main Menu---------\n");
		printf("\n1. Insert the data at start");
		printf("\n2. Insert the data at end");
		printf("\n3. Insert the data at specific location");
		printf("\n4. Delete from start");
		printf("\n5. Delete from end");
		printf("\n6. Delete at specific");
		printf("\n7. Display the list");
		printf("\n8. Exit");
		printf("\nEnter your option:");
		scanf("%d",&option);
		
		switch(option)
		{
			case 1:
				insert_Start();  
				break;
			case 2:
				insert_end();
				break;
			case 3:
				insert_specific();
				break;
			case 4:
				delete_start();
			    break;
			case 5:  
            	delete_end();
            	break;  
            case 6:  
            	delete_specific();
            	break;   
            case 7:  
            	display();  
            	break;
            case 8:  
            	exit(0);
            	break;
			default:
				printf("Please enter the valid option....");
		}
	}
}