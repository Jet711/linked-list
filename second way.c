//with two array : one for data and one for stroing pointer

#include<stdio.h>
#include<stdlib.h>
#define MAX 10000 
struct
{
    int list[MAX];
    int data;     
    int pos;           
    int length;     
}l;

void create()
{
    int data;
    int flag=1;
    while(flag==1)
    {
        printf("Enter Data : ");
        scanf("%d", &data);
        if(data<=0)
        {
          printf("\nNumber not accepted");
        }
        else
        l.list[l.length] = data;
        l.length++;
        printf("\nTo insert another element press '1' : ");
        scanf("%d", &flag);
    }
}     

void insert(int data, int pos)
{
    int i;
    if (pos == 0)
    {
        printf("\nCannot insert an element at 0th position");
        
        return;
    }

    if (pos-1 > l.length)
    {
        printf("\nOnly %d elements exit. Cannot insert at %d position", l.length, pos);
        
    }
    else
    {
        for (i=l.length; i>=pos-1; i--)
        {
            l.list[i+1] = l.list[i];
        }
        l.list[pos-1] = data;
        l.length++;
    }
}     
void delet(int pos)
{
    int i;
    if(pos == 0)
    {
        printf("\nCannot delete at an element 0th position");
      
        return;
    }
    if (pos > l.length)
    {
        printf("\n\n Only %d elements exit. Cannot delete %d", l.length, pos);
        return;
    }
    for (i=pos-1; i<l.length; i++)
    {
        l.list[i] = l.list[i+1];
    }
    l.length--;
}      
 
void display() 
{
  int i;
  for (i=0; i<l.length; i++)
  {
    printf("Element %d : %d \n", i, l.list[i]);
  }    
}
int main()
{
  int data,loc;
  int option;
  l.length = 0;
	while(option!=6)
	{
		printf("\n-------Main Menu---------\n");
		printf("\n1. Create");
		printf("\n2. Insert");
		printf("\n3. Delete");
		printf("\n4. Length");
		printf("\n5. Display");
		printf("\n6. Exit");
		printf("\nEnter your option:");
		scanf("%d",&option);
		
		switch(option)
		{
			case 1:
        		l.length = 0;
        		create();  
				break;
			case 2:
        		if(l.length != MAX)
        		{		
        			printf("Enter New data: ");
        			scanf("%d", &data);
          			if(data<=0)
          			{
            			printf("\nNumber not accepted");
          			}			
          			else
          			{
            			printf("\nEnter the index location : ");
            			scanf("%d", &loc);
            			insert(data, loc);
          			}
        		}
        		else
        		{
          			printf("\nCan't insert List is full....");
        		}
				break;
			case 3:
        		if (l.length != 0)
        		{
          			printf("\nEnter the position of element to be deleted : ");
          			scanf("%d", &loc);
          			delet(loc);
        		}
        		else
        		{
          			printf("\nList is Empty.");
        		}
				break;
			case 4:
        		printf("\nLength of the list is  %d", l.length);
			  	break;  
      		case 5:  
        		display();     	
        		break;   
      		case 6:  
        		exit(0);   	 
        		break;
			default:
				printf("\nPlease enter the valid option....");
		}
	}
}